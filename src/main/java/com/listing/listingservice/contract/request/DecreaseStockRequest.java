package com.listing.listingservice.contract.request;

public class DecreaseStockRequest {
    private int quantity;

    public DecreaseStockRequest(int quantity) {
        this.quantity = quantity;
    }

    public DecreaseStockRequest() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
